<?php
/**
 * @file Do Admin settings
 */

/**
 * Admin settings form
 */
function do_settings() {
  drupal_add_js(drupal_get_path('module', 'do') . '/js/do.admin.js');

  // prepare some default values
  $do_trigger = variable_get('do_trigger', '32'); // @todo convert to legible format

  $form = array();

  $form['do_shortcut'] = array(
    '#type' => 'fieldset',
    '#title' => t('Keyboard shortcut'),
    '#description' => t('Select a keyboard modifier to launch Do. The default is Ctrl + Space.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $form['do_shortcut']['do_modifier'] = array(
    '#type'          => 'radios',
    '#name'          => 'do_modifier',
    '#title'         => t('Do Keyboard Modifier'),
    '#description'   => t('Hold the modifier key to start the shortcut'),
    '#required'      => TRUE,
    '#default_value' => variable_get('do_modifier', 'ctrl'),
    '#options'       => array('ctrl' => 'ctrl', 'shift' => 'shift', 'alt' => 'alt', 'meta' => 'meta'),
  );

  $form['do_shortcut']['do_trigger'] = array(
    '#type'          => 'textfield',
    '#name'          => 'do_trigger',
    '#title'         => t('Do Keyboard Trigger'),
    '#description'   => t('Press the trigger key while holding the modifier key'),
    '#required'      => TRUE,
    '#default_value' => $do_trigger,
    '#maxlength'     => 20,
  );

  return system_settings_form($form);
}
