<?php
/**
 * @file Do Ajax handler
 */

/**
 * Ajax request handler
 */
function _do_ajax() {
  // horray we have our own hooks
  $hooks = module_invoke_all('do');
  $response = array('ok' => FALSE);
  
  if (isset($_POST['op'])) {
    $ops = explode(' ', $_POST['op']);
    $op = array_shift($ops);

    // 1 word is easier
    if (in_array($op, $hooks)) {
      $hook_return = call_user_func_array($op, $ops);
      $response = array_merge($response, (array) $hook_return);
    } else if (do_drupal_path_exists($op)) {
      $response['ok'] = TRUE;
      $response['path'] = $op;
    }
  }
    
  drupal_json($response);
  die;
}

/**
 * Autocomplete handler
 */
function _do_autocomplete($str) {
  $suggestions = array();
  
  // ..
  
  $response = array(
    'ok' => TRUE,
    'data' => $suggestions
  );
  
  drupal_json($response);
  die;
}

/**
 * Implementation of hook_do().
 * Default Do actions
 */
function do_do() {
  module_load_include('inc
  return array(
    '_do_flush',
  );
}

/**
 * Determines if path is valid
 * @param string
 * @return int
 */
function do_drupal_path_exists($op) {
  // @todo try to check aliases / sources using drupal_lookup_path()
  $query = "SELECT path FROM {menu_router} WHERE path = '%s'";
  $resource = db_query($query, $op);
  $result = db_result($resource);
  return $result;
}
