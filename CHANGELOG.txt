6.x-1.0-alpha1 - 2011-04-23
----------------------------
- initial preview release
- basic module hooks
- simple modal & event handling
- basic default stylesheet
- still debating whether or not to use CTools
