CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation


INTRODUCTION
------------

Author:
* Alex Weber (alexweber15)

Do was inspired by Gnome Do (http://do.davebsd.com/) and is an extensible tool for quick & painless Drupal site administration and development.

Development has started literally 2 hours ago and the project is barely in ALPHA stage. The intent of this release is merely as a preview to attract possible collaborators.

INSTALLATION
------------

1. Copy the headjs directory to your sites/all/modules or sites/all/modules/contrib directory.

2. Enable the module at Administer >> Site building >> Modules.

3. Visit Administer >> Site Configuration >> Do to configure the module (optional)

4. That's it!
