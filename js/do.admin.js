/**
 * @file Do Drupal admin settings form behaviors
 */

Drupal.behaviors.do_admin = function(context) {
  $('#edit-do-trigger', context).bind('keyup', function(e) {
    // @todo allow changing keyboard shortcut
  });
}
