/**
 * @file Do core & events
 */

/*
 * Do Core
 */

Drupal.Do = Drupal.Do || { overlay: null, buffer = '' };

Drupal.Do.open = function() {
  if (!Drupal.Do.overlay) {
    Drupal.Do.init();
  }

  Drupal.Do.overlay.show();
  $('#do_input', Drupal.Do.overlay).focus();
};

Drupal.Do.close = function() {
  Drupal.Do.buffer = '';
  $('#do_input', Drupal.Do.overlay).val('');
  Drupal.Do.overlay.hide();
};

Drupal.Do.msg = function(msg, ok) {
  // @todo display messages
  alert(msg);
};

Drupal.Do.autocomplete = function(code) {
  var letter = String.fromCharCode(code);
  Drupal.Do.buffer += letter;
  // @todo query server
};

Drupal.Do.execute = function() {
  var $input = $('#do_input', Drupal.Do.overlay),
      val = $input.val();

  if (val !== '') {
    $.post('/do_ajax', 'op=' + val, function(response) {
      if (response.path) {
        window.location = '/' + response.path;
      } else if (response.msg) {
        Drupal.Do.msg(response.msg, response.ok);
      }

      Drupal.Do.buffer = '';
      $input.val('');
      return false;
    }, 'json');
  }
};

Drupal.Do.init = function(context) {
  Drupal.Do.overlay = $('<div id="do_overlay" />');
  var $modal = $('<div id="do_modal" />'),
      $input = $('<input type="text" id="do_input" />');

  $input.bind('keyup', function(e) {
    var code = e.keyCode ? e.keyCode : e.which;
    if (code === 13) {
      Drupal.Do.execute();
    }else if (code === 27) {
      Drupal.Do.close();
    } else {
      Drupal.Do.autocomplete(code);
    }
  });

  $modal.append($input);
  Drupal.Do.overlay.append($modal).click(function() {
    $input.focus();
  });
  $('body', context).append(Drupal.Do.overlay);
};

/*
 * Do Events
 */

Drupal.behaviors.do_trigger = function(context) {
  // bind keyboard shortcut
  $(document, context).bind('keyup', function(e) {
    var code = e.keyCode ? e.keyCode : e.which;
    if (code == Drupal.settings.Do.trigger) {
      var key = Drupal.settings.Do.modifier + 'Key';
      // nifty trick to avoid a bunch of if statements - this will probably break in some browsers
      if (e[key] === true) {
        Drupal.Do.open();
        e.cancelBubble = true; // for IE
        e.returnValue = false;
        if (e.stopPropagation) {
          e.stopPropagation();
          e.preventDefault();
        }
        return false;
      }
    }
  });
};
